package Pl3xPerms;

import java.io.File;
import java.util.UUID;

import net.pl3x.pl3xlibs.BetterPluginInfo;
import net.pl3x.pl3xlibs.Logger;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.configuration.BaseConfig;
import net.pl3x.pl3xperms.commands.Perm;
import net.pl3x.pl3xperms.configuration.GroupConfig;
import net.pl3x.pl3xperms.configuration.UserConfig;
import net.pl3x.pl3xperms.runnables.StartMetrics;
import PluginReference.MC_Player;
import PluginReference.MC_Server;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

public class MyPlugin extends PluginBase {
	private static MyPlugin instance;
	private MC_Server server;
	private BaseConfig config;
	private BaseConfig dataConfig;
	private Logger logger;

	public PluginInfo getPluginInfo() {
		PluginInfo info = new PluginInfo();
		info.name = "Pl3xPerms";
		info.version = "0.3";
		info.description = "Permissions system for RainbowMod";
		info.optionalData.put("plugin-directory", "plugins_mod" + File.separator + info.name + File.separator);
		return info;
	}

	public static MyPlugin getInstance() {
		return instance;
	}

	@Override
	public void onStartup(MC_Server argServer) {
		server = argServer;
		instance = this;
		init();
		getServer().registerCommand(new Perm(this));
		getLogger().info(getPluginInfo().name + " v" + getPluginInfo().version + " by BillyGalbreath is now enabled.");
	}

	@Override
	public void onShutdown() {
		disable(true);
		getLogger().info("Plugin disabled.");
	}

	public void init() {
		getConfig();
		GroupConfig.getGroup(this, "default"); // create default group in case it doesnt exist
		GroupConfig.loadAllGroups(new File(new BetterPluginInfo(getPluginInfo()).getPluginDirectory() + File.separator + "groups"));
		try {
			for (MC_Player target : getServer().getPlayers()) {
				UserConfig.getUser(this, target.getUUID());
			}
		} catch (NullPointerException ignore) {
		}
		Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new StartMetrics(this), 100);
	}

	public void disable(boolean save) {
		Pl3xLibs.getScheduler().cancelAllRunnables(getPluginInfo().name);
		UserConfig.unloadAllUsers(save);
		GroupConfig.unloadAllGroups(save);
		config = null;
		logger = null;
	}

	public void reload() {
		disable(false);
		init();
	}

	public MC_Server getServer() {
		return server;
	}

	public BaseConfig getConfig() {
		if (config == null) {
			config = new BaseConfig(MyPlugin.class, getPluginInfo(), "", "config.ini");
			config.load();
		}
		return config;
	}

	public BaseConfig getDataConfig() {
		if (dataConfig == null) {
			dataConfig = new BaseConfig(MyPlugin.class, getPluginInfo(), "", "datastore.ini");
			dataConfig.load(false);
		}
		return dataConfig;
	}

	public Logger getLogger() {
		if (logger == null) {
			logger = Pl3xLibs.getLogger(getPluginInfo());
		}
		return logger;
	}

	@Override
	public void onPlayerJoin(MC_Player player) {
		UserConfig.getUser(this, player.getUUID());
	}

	@Override
	public void onPlayerLogout(String playerName, UUID uuid) {
		UserConfig.unloadUser(uuid, true);
	}

	@Override
	public Boolean onRequestPermission(String playerKey, String permission) {
		UUID uuid;
		try {
			uuid = UUID.fromString(playerKey);
		} catch (Exception e1) {
			try {
				uuid = UUID.fromString(getServer().getPlayerUUIDFromName(playerKey));
			} catch (Exception e2) {
				return false;
			}
		}
		UserConfig config = UserConfig.getUser(this, uuid);
		boolean result = config.hasPerm(permission);
		if (getConfig().getBoolean("debug-mode", false)) {
			getLogger().debug("Permissions Check Performed! player: " + playerKey + " permission: " + permission + " result: " + ((result) ? "passed" : "failed"));
		}
		return result;
	}
}
