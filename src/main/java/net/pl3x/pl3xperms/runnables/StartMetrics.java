package net.pl3x.pl3xperms.runnables;

import java.io.IOException;

import net.pl3x.pl3xlibs.MetricsLite;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.scheduler.Pl3xRunnable;
import Pl3xPerms.MyPlugin;

public class StartMetrics extends Pl3xRunnable {
	private MyPlugin plugin;
	private MetricsLite metrics;

	public StartMetrics(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		if (plugin.getConfig().getBoolean("debug-mode", false)) {
			plugin.getLogger().debug("Starting metrics!");
		}
		try {
			metrics = new MetricsLite(MyPlugin.getInstance().getPluginInfo());
			metrics.start();
		} catch (IOException e) {
			Pl3xLibs.getLogger(plugin.getPluginInfo()).error("Failed to start Metrics: " + e.getMessage());
		}
	}
}
