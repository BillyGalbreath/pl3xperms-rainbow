package net.pl3x.pl3xperms.commands.group;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.commands.Command;
import net.pl3x.pl3xperms.configuration.GroupConfig;
import Pl3xPerms.MyPlugin;
import PluginReference.MC_Player;

public class ListPerms extends Command {
	private MyPlugin plugin;

	public ListPerms(MyPlugin plugin) {
		super("listperms", "List a group's permissions", "perms.command.perm.group.listperms", "&7/perm group listperms &e[&7group&e]");
		this.plugin = plugin;
	}

	@Override
	public List<String> getTabCompletionList(LinkedList<String> args, MC_Player player) {
		if (args.size() == 1) {
			return GroupConfig.getMatchingGroupNames(args.peek());
		}
		return null;
	}

	@Override
	public void handleCommand(LinkedList<String> args, MC_Player player) throws Exception {
		if (args.size() != 1) {
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		String name = args.pop();
		if (!GroupConfig.groupExists(name)) {
			Pl3xLibs.sendMessage(player, "&4That group does not exist!");
			return;
		}
		GroupConfig group = GroupConfig.getGroup(plugin, name);
		HashMap<String, Boolean> groupPerms = group.getPerms();
		Set<String> nodes = new HashSet<String>();
		if (groupPerms != null) {
			for (String node : groupPerms.keySet()) {
				if (group.getPerms().get(node)) {
					nodes.add(node); // group perm = true
				}
			}
		}
		String permsList = "none";
		if (nodes != null && !nodes.isEmpty()) {
			permsList = Pl3xLibs.join(nodes, "&e, &7", 0);
		}
		Pl3xLibs.sendMessage(player, "&dPermissions for group &7" + group.getName() + "&d: &e[&7" + permsList + "&e]");
	}
}
