package net.pl3x.pl3xperms.commands.group;

import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.commands.Command;
import net.pl3x.pl3xperms.configuration.GroupConfig;
import Pl3xPerms.MyPlugin;
import PluginReference.MC_Player;

public class UnSetPerm extends Command {
	private MyPlugin plugin;

	public UnSetPerm(MyPlugin plugin) {
		super("unsetperm", "Unset a group permission", "perms.command.perm.group.unsetperm", "&7/perm group unsetperm &e[&7group&e] [&7perm.node&e]");
		this.plugin = plugin;
	}

	@Override
	public List<String> getTabCompletionList(LinkedList<String> args, MC_Player player) {
		if (args.size() == 1) {
			return GroupConfig.getMatchingGroupNames(args.peek());
		}
		return null;
	}

	@Override
	public void handleCommand(LinkedList<String> args, MC_Player player) throws Exception {
		if (args.size() != 2) {
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		String name = args.pop();
		String node = args.pop();
		if (!GroupConfig.groupExists(name)) {
			Pl3xLibs.sendMessage(player, "&4That group does not exist!");
			return;
		}
		GroupConfig group = GroupConfig.getGroup(plugin, name);
		if (node == null || node.equals("")) {
			Pl3xLibs.sendMessage(player, "&4Improper permission node specified!");
			return;
		}
		group.unsetPerm(node);
		Pl3xLibs.sendMessage(player, "&dPermission unset for group &7" + group.getName() + " &e[&7" + node + "&e]");
	}
}
