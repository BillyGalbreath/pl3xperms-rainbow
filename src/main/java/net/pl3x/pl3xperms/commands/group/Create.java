package net.pl3x.pl3xperms.commands.group;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.commands.Command;
import net.pl3x.pl3xperms.configuration.GroupConfig;
import Pl3xPerms.MyPlugin;
import PluginReference.MC_Player;

public class Create extends Command {
	private MyPlugin plugin;

	public Create(MyPlugin plugin) {
		super("create", "Create a new group", "perms.command.perm.group.create", "&7/perm group create &e[&7group&e]");
		this.plugin = plugin;
	}

	@Override
	public List<String> getTabCompletionList(LinkedList<String> args, MC_Player player) {
		return null;
	}

	@Override
	public void handleCommand(LinkedList<String> args, MC_Player player) throws Exception {
		if (args.size() != 1) {
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		String name = args.pop();
		if (name == null || name.equals("") || !Pattern.compile("^[a-zA-Z0-9-_]{3,30}$").matcher(name).find()) {
			Pl3xLibs.sendMessage(player, "&4Invalid group name given!");
			return;
		}
		if (GroupConfig.groupExists(name)) {
			Pl3xLibs.sendMessage(player, "&4That group is already created!");
			return;
		}
		GroupConfig group = GroupConfig.getGroup(plugin, name);
		group.save();
		Pl3xLibs.sendMessage(player, "&dGroup has been created.");
	}
}
