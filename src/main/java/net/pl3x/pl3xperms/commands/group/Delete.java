package net.pl3x.pl3xperms.commands.group;

import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.commands.Command;
import net.pl3x.pl3xperms.configuration.GroupConfig;
import Pl3xPerms.MyPlugin;
import PluginReference.MC_Player;

public class Delete extends Command {
	private MyPlugin plugin;

	public Delete(MyPlugin plugin) {
		super("delete", "Delete a group", "perms.command.perm.group.delete", "&7/perm group delete &e[&7group&e]");
		this.plugin = plugin;
	}

	@Override
	public List<String> getTabCompletionList(LinkedList<String> args, MC_Player player) {
		if (args.size() == 1) {
			return GroupConfig.getMatchingGroupNames(args.peek());
		}
		return null;
	}

	@Override
	public void handleCommand(LinkedList<String> args, MC_Player player) throws Exception {
		if (args.size() != 1) {
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		String name = args.pop();
		if (!GroupConfig.groupExists(name)) {
			Pl3xLibs.sendMessage(player, "&4That group does not exist!");
			return;
		}
		GroupConfig group = GroupConfig.getGroup(plugin, name);
		if (!group.delete()) {

		}
		Pl3xLibs.sendMessage(player, "&dGroup has been deleted.");
	}
}
