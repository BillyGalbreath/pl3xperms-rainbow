package net.pl3x.pl3xperms.commands;

import net.pl3x.pl3xlibs.commands.BaseCommand;
import net.pl3x.pl3xperms.commands.group.Create;
import net.pl3x.pl3xperms.commands.group.Delete;
import net.pl3x.pl3xperms.commands.group.Inherit;
import net.pl3x.pl3xperms.commands.group.ListPerms;
import net.pl3x.pl3xperms.commands.group.SetPerm;
import net.pl3x.pl3xperms.commands.group.UnSetPerm;
import Pl3xPerms.MyPlugin;

public class Group extends BaseCommand {
	public Group(MyPlugin plugin) {
		super("group", "Manage group permissions", "perms.command.perm.group", null);
		registerSubcommand(new Create(plugin));
		registerSubcommand(new Delete(plugin));
		registerSubcommand(new Inherit(plugin));
		registerSubcommand(new ListPerms(plugin));
		registerSubcommand(new SetPerm(plugin));
		registerSubcommand(new UnSetPerm(plugin));
	}
}
