package net.pl3x.pl3xperms.commands;

import net.pl3x.pl3xlibs.commands.BaseCommand;
import net.pl3x.pl3xperms.commands.player.Demote;
import net.pl3x.pl3xperms.commands.player.ListPerms;
import net.pl3x.pl3xperms.commands.player.Promote;
import net.pl3x.pl3xperms.commands.player.SetGroup;
import net.pl3x.pl3xperms.commands.player.SetPerm;
import net.pl3x.pl3xperms.commands.player.TestPerm;
import net.pl3x.pl3xperms.commands.player.UnSetPerm;
import Pl3xPerms.MyPlugin;

public class Player extends BaseCommand {
	public Player(MyPlugin plugin) {
		super("player", "Manage player permissions", "perms.command.perm.player", null);
		registerSubcommand(new Demote(plugin));
		registerSubcommand(new ListPerms(plugin));
		registerSubcommand(new Promote(plugin));
		registerSubcommand(new SetGroup(plugin));
		registerSubcommand(new SetPerm(plugin));
		registerSubcommand(new TestPerm(plugin));
		registerSubcommand(new UnSetPerm(plugin));
	}
}
