package net.pl3x.pl3xperms.commands.player;

import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.commands.Command;
import net.pl3x.pl3xperms.configuration.UserConfig;
import Pl3xPerms.MyPlugin;
import PluginReference.MC_Player;

public class SetPerm extends Command {
	private MyPlugin plugin;

	public SetPerm(MyPlugin plugin) {
		super("setperm", "Set a player permission", "perms.command.perm.player.setperm", "&7/perm player setperm &e[&7player&e] [&7perm.node&e] [&7true&e|&7false&e]");
		this.plugin = plugin;
	}

	@Override
	public List<String> getTabCompletionList(LinkedList<String> args, MC_Player player) {
		if (args.size() == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args.peek());
		}
		return null;
	}

	@Override
	public void handleCommand(LinkedList<String> args, MC_Player player) throws Exception {
		if (args.size() != 3) {
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		String name = args.pop();
		String node = args.pop();
		String value = args.pop();
		MC_Player target = Pl3xLibs.getOfflinePlayer(name);
		if (target == null) {
			Pl3xLibs.sendMessage(player, "&4Player not found!");
			return;
		}
		if (node == null || node.equals("")) {
			Pl3xLibs.sendMessage(player, "&4Improper permission node specified!");
			return;
		}
		Boolean bool = null;
		if (value.equalsIgnoreCase("true")) {
			bool = true;
		}
		if (value.equalsIgnoreCase("false")) {
			bool = false;
		}
		if (bool == null) {
			Pl3xLibs.sendMessage(player, "&4Only true or false values are allowed!");
			Pl3xLibs.sendMessage(player, "&4If you are trying to unset a perm, try using the &7unsetperm &4subcommand!");
			return;
		}
		UserConfig config = UserConfig.getUser(plugin, target.getUUID());
		config.setPerm(node, bool);
		Pl3xLibs.sendMessage(player, "&dPermission set for &7" + target.getCustomName() + " &e[&7" + node + "&e:&7 " + bool.toString() + "&e]");
	}
}
