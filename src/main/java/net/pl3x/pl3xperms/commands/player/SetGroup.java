package net.pl3x.pl3xperms.commands.player;

import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.commands.Command;
import net.pl3x.pl3xperms.configuration.GroupConfig;
import net.pl3x.pl3xperms.configuration.UserConfig;
import Pl3xPerms.MyPlugin;
import PluginReference.MC_Player;

public class SetGroup extends Command {
	private MyPlugin plugin;

	public SetGroup(MyPlugin plugin) {
		super("setgroup", "Set the player to a group", "perms.command.perm.player.setgroup", "&7/perm player setgroup &e[&7player&e] &e[&7group&e]");
		this.plugin = plugin;
	}

	@Override
	public List<String> getTabCompletionList(LinkedList<String> args, MC_Player player) {
		if (args.size() == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args.peek());
		}
		return null;
	}

	@Override
	public void handleCommand(LinkedList<String> args, MC_Player player) throws Exception {
		if (args.size() != 2) {
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		String name = args.pop();
		String groupName = args.pop();
		MC_Player target = Pl3xLibs.getOfflinePlayer(name);
		if (target == null) {
			Pl3xLibs.sendMessage(player, "&4Player not found!");
			return;
		}
		if (groupName == null || groupName.equals("")) {
			Pl3xLibs.sendMessage(player, "&4Improper group specified!");
			return;
		}
		groupName = groupName.toLowerCase();
		if (!GroupConfig.groupExists(groupName)) {
			Pl3xLibs.sendMessage(player, "&4That group does not exist!");
			return;
		}
		UserConfig config = UserConfig.getUser(plugin, target.getUUID());
		if (config.getGroupName().equals(groupName)) {
			Pl3xLibs.sendMessage(player, "&4Player is already in that group!");
			return;
		}
		config.setGroup(groupName);
		Pl3xLibs.sendMessage(player, "&dGroup set for &7" + target.getCustomName() + " &e[&7" + groupName + "&e]");
	}
}
