package net.pl3x.pl3xperms.commands.player;

import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.commands.Command;
import Pl3xPerms.MyPlugin;
import PluginReference.MC_Player;

public class Demote extends Command {
	private MyPlugin plugin;

	public Demote(MyPlugin plugin) {
		super("demote", "Demote a player", "perms.command.perm.player.demote", "&7/perm player demote &e[&7player&e]");
		this.plugin = plugin;
	}

	@Override
	public List<String> getTabCompletionList(LinkedList<String> args, MC_Player player) {
		return null;
	}

	@Override
	public void handleCommand(LinkedList<String> args, MC_Player player) throws Exception {
		Pl3xLibs.sendMessage(player, "&c&oThis command has not been implemented yet!");
	}
}
