package net.pl3x.pl3xperms.commands.player;

import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.commands.Command;
import net.pl3x.pl3xperms.configuration.UserConfig;
import Pl3xPerms.MyPlugin;
import PluginReference.MC_Player;

public class TestPerm extends Command {
	private MyPlugin plugin;

	public TestPerm(MyPlugin plugin) {
		super("testperm", "Test if player has a permission", "perms.command.perm.player.testperm", "&7/perm player testperm &e[&7player&e] [&7perm.node&e]");
		this.plugin = plugin;
	}

	@Override
	public List<String> getTabCompletionList(LinkedList<String> args, MC_Player player) {
		if (args.size() == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args.peek());
		}
		return null;
	}

	@Override
	public void handleCommand(LinkedList<String> args, MC_Player player) throws Exception {
		if (args.size() != 2) {
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		String name = args.pop();
		String node = args.pop();
		MC_Player target = Pl3xLibs.getOfflinePlayer(name);
		if (target == null) {
			Pl3xLibs.sendMessage(player, "&4Player not found!");
			return;
		}
		if (node == null || node.equals("")) {
			Pl3xLibs.sendMessage(player, "&4Improper permission node specified!");
			return;
		}
		UserConfig config = UserConfig.getUser(plugin, target.getUUID());
		if (config.hasPerm(node)) {
			Pl3xLibs.sendMessage(player, "&7" + target.getCustomName() + " &dhas permission &e[&7" + node + "&e]");
		} else {
			Pl3xLibs.sendMessage(player, "&7" + target.getCustomName() + " &ddoes not have permission &e[&7" + node + "&e]");
		}
	}
}
