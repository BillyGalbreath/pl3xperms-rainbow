package net.pl3x.pl3xperms.configuration;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.pl3x.pl3xlibs.BetterPluginInfo;
import net.pl3x.pl3xlibs.configuration.BaseConfig;
import Pl3xPerms.MyPlugin;

public class GroupConfig extends BaseConfig {
	private static final HashMap<String, GroupConfig> groups = new HashMap<String, GroupConfig>();

	public static GroupConfig getGroup(MyPlugin plugin, String name) {
		String savedname = name.toLowerCase();
		synchronized (groups) {
			if (groups.containsKey(savedname)) {
				return groups.get(savedname);
			}
			GroupConfig group = new GroupConfig(plugin, name);
			groups.put(savedname, group);
			return group;
		}
	}

	public static boolean groupExists(String name) {
		synchronized (groups) {
			return groups.containsKey(name.toLowerCase());
		}
	}

	public static List<String> getMatchingGroupNames(String name) {
		List<String> matches = new ArrayList<String>();
		if (name == null) {
			name = "";
		}
		name = name.toLowerCase().trim();
		synchronized (groups) {
			for (String groupName : groups.keySet()) {
				if (groupName.toLowerCase().startsWith(name)) {
					matches.add(groupName);
				}
			}
		}
		return matches;
	}

	public static void unloadGroup(String name, boolean save) {
		name = name.toLowerCase();
		synchronized (groups) {
			if (!groups.containsKey(name)) {
				return;
			}
			groups.get(name).discard(save);
		}
	}

	public static void unloadAllGroups(boolean save) {
		Collection<GroupConfig> oldGroups = new ArrayList<GroupConfig>();
		synchronized (groups) {
			oldGroups.addAll(groups.values());
			for (GroupConfig group : oldGroups) {
				group.discard(save);
			}
		}
	}

	public static void saveAllGroups() {
		synchronized (groups) {
			for (GroupConfig group : groups.values()) {
				group.save();
			}
		}
	}

	public static void loadAllGroups(File dir) {
		for (File file : dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".ini");
			}
		})) {
			String name = file.getName();
			int ext = name.lastIndexOf(".ini");
			if (ext != -1) {
				name = name.substring(0, ext);
			}
			GroupConfig.getGroup(MyPlugin.getInstance(), name);
		}
	}

	private MyPlugin plugin;
	private String name;
	private Integer path;
	private HashMap<String, Boolean> perms = new HashMap<String, Boolean>();

	public GroupConfig(MyPlugin plugin, String name) {
		super(MyPlugin.class, plugin.getPluginInfo(), "groups" + File.separator, name.toLowerCase() + ".ini");
		this.plugin = plugin;
		this.name = name;
		load(false);
		init();
	}

	private void init() {
		perms.clear();
		String inherit = map.get("inherit");
		if (inherit != null && !inherit.equals("") && !name.equalsIgnoreCase("default")) {
			for (Map.Entry<String, Boolean> entry : GroupConfig.getGroup(plugin, inherit).getPerms().entrySet()) {
				if (!perms.containsKey(entry.getKey())) {
					perms.put(entry.getKey(), entry.getValue());
				}
			}
		}
		String path = map.get("path");
		if (path != null) {
			try {
				this.path = Integer.valueOf(path);
			} catch (NumberFormatException e) {
				plugin.getLogger().error("Group path set incorrectly: " + name.toLowerCase() + ".ini");
				discard();
			}
		}
		for (String key : map.keySet()) {
			if (key.equals("inherit") || key.equals("path")) {
				continue;
			}
			String value = map.get(key);
			if (value == null || value.equals("")) {
				continue;
			}
			Boolean bool = null;
			if (value.equalsIgnoreCase("true")) {
				bool = true;
			}
			if (value.equalsIgnoreCase("false")) {
				bool = false;
			}
			if (bool == null) {
				continue; // must specifically be set to "true" or "false" in order to be considered valid
			}
			perms.put(key, bool);
		}
	}

	public String getName() {
		return name;
	}

	public Integer getPromotionPath() {
		return path;
	}

	public GroupConfig getInheritedGroup() {
		String inherit = map.get("inherit");
		if (inherit == null || inherit.equals("")) {
			return null;
		}
		return GroupConfig.getGroup(plugin, inherit);
	}

	public void setInheritedGroup(String inherit) {
		set("inherit", inherit);
		save();
		init();
	}

	public void unsetInheritedGroup() {
		remove("inherit");
		save();
		init();
	}

	public HashMap<String, Boolean> getPerms() {
		return perms;
	}

	public void setPerm(String key, Boolean value) {
		perms.put(key, value);
		set(key, value ? "true" : "false");
		save();
		init();
	}

	public void unsetPerm(String key) {
		perms.remove(key);
		remove(key);
		save();
		init();
	}

	public Boolean hasPerm(String node) {
		boolean hasPerm = false;
		if (perms.containsKey(node)) {
			hasPerm = perms.get(node);
		}
		return hasPerm;
	}

	public void discard() {
		discard(false);
	}

	public void discard(boolean save) {
		if (save) {
			save();
		}
		synchronized (groups) {
			if (!groups.containsKey(name.toLowerCase())) {
				return;
			}
			groups.remove(name.toLowerCase());
		}
	}

	public boolean delete() {
		File file = new File(new BetterPluginInfo(plugin.getPluginInfo()).getPluginDirectory() + "groups" + File.separator + name + ".ini");
		discard();
		return file.delete();
	}
}
